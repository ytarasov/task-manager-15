package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(Project project) throws AbstractException;

    Project create(String name) throws AbstractException;

    Project create(String name, String description) throws AbstractException;

    Project create(String name, Status status) throws NameEmptyException;

    Project create(String name, String description, Status status) throws AbstractException;

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id) throws AbstractException;

    Project findOneByIndex(Integer index) throws IndexIncorrectException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

    Project removeById(String id) throws AbstractException;

    Project removeByIndex(Integer index) throws AbstractException;
}
