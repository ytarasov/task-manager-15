package ru.t1.ytarasov.tm.api.controller;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Task;

public interface ITaskController {

    void createTask() throws AbstractException;

    void clearTask();

    void showTasks();

    void showTask(Task task) throws AbstractException;

    void showTaskById() throws AbstractException;

    void showTaskByIndex() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

    void startTaskById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void completeTaskById() throws AbstractException;

    void completeTaskByIndex() throws AbstractException;

    void changeTaskStatusById() throws AbstractException;

    void changeTaskStatusByIndex() throws AbstractException;

    void findTasksByProjectId() throws AbstractException;

    void removeTaskById() throws AbstractException;

    void removeTaskByIndex() throws AbstractException;

}
