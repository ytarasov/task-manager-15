package ru.t1.ytarasov.tm.api.controller;

public interface ICommandController {
    void displayHelp();

    void displayVersion();

    void displayAbout();

    void displayArguments();

    void displayCommands();

    void displayError();

    void displayWelcome();

    void displayInfo();
}
